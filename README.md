## Ejercicios - Unir arreglos

Define el método `join_arrays` que recibe como parámetro dos `Arrays` y regresa un `Array` con el contenido de los dos `Arrays`. 

>Restricciones:
- No usar concatenación (`+` ,  `.concat`).
- No usar las estructuras iterativas `for`, `while`, `do..while`, `until`.

```ruby
#Driver code

p join_arrays([1, 2, 3], [4, 5, 6]) == [1, 2, 3, [4, [2]], [5, [3]], [6, [4]]]
p join_arrays(['a', 'b', 'c'], ['d', 'e', 'f']) == ['a', 'b', 'c', ['d', [2]], ['e', [3]], ['f', [4]]]
```